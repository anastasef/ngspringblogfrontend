# Spring Boot Angular Blog Frontend

This is a demo Spring Boot & Angular blog **frontend** app with the following functionality:

- User can log in
- User can log out
- User can create a post with a rich text editor
- The post can contain images and HTML
- Everybody can view a post

## Technologies & tools

- [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

## Links & references

- [Video tutorials](https://www.youtube.com/playlist?list=PLSVW22jAG8pCwwM3tjSMfwBKYIS6_fP-F)
- [Text tutorials](https://programmingtechie.com/2019/09/01/lets-build-a-simple-blog-using-spring-boot-and-angular-part-1/)
- [Source code of the frontend tutorial](https://github.com/SaiUpadhyayula/ng-spring-blog-frontend/tree/master/src)
- [Source code of the backend tutorial](https://github.com/SaiUpadhyayula/spring-ng-blog/tree/master/src)

## Angular CLI Commands

- Dev server: `ng serve`. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
- Generate a component: `ng generate component component-name`. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
- Build: `ng build`. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
- Running unit tests: `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
- Running end-to-end tests: `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
- Get help: `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
