import { RegistrationPayload } from './registration-payload';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {LoginPayload} from './login-payload';
import {JwtAuthResponse} from './jwt-auth-response';
import {map} from 'rxjs/operators';
import {LocalStorageService} from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'http://localhost:8080/api/auth/';

  constructor(private httpClient: HttpClient,
              private localStorageService: LocalStorageService) { }

  register(registrationPayload: RegistrationPayload): Observable<any> {
    return this.httpClient.post(this.url + 'signup', registrationPayload );
  }

  login(loginPayload: LoginPayload): Observable<boolean> {
    return this.httpClient.post<JwtAuthResponse>(this.url + 'login', loginPayload )
      .pipe(map( data => {
        // store the token and the username in the local storage
        this.localStorageService.store('authToken', data.authToken);
        this.localStorageService.store('username', data.username);
        return true;
      }));
  }
}
