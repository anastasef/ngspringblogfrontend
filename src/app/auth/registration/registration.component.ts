import { AuthService } from '../auth.service';
import { RegistrationPayload } from '../registration-payload';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  registrationPayload: RegistrationPayload;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {

    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: '',
      password: '',
      confirmPassword: ''
    });

    this.registrationPayload = {
      username: '',
      email: '',
      password: '',
      confirmPassword: ''
    };
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.registrationPayload.username = this.registerForm.get('username').value;
    this.registrationPayload.email = this.registerForm.get('email').value;
    this.registrationPayload.password = this.registerForm.get('password').value;
    this.registrationPayload.confirmPassword = this.registerForm.get('confirmPassword').value;

    this.authService.register(this.registrationPayload).subscribe(data => {
      console.log('register success');
      this.router.navigateByUrl('/registration-success');
    }, error => {
      console.log('register failed');
    });
  }

}
