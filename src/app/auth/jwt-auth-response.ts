/**
 * Server response to POST /api/auth/login
 */
export class JwtAuthResponse {
  authToken: string; // authentication token
  username; string;
}
